.. timeutils documentation master file, created by
   sphinx-quickstart on Thu Sep 14 23:29:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to timeutils's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   timeutils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
