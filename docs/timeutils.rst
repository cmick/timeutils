timeutils package
=================

Submodules
----------

.. toctree::

   timeutils.stopwatch
   timeutils.timespan

Module contents
---------------

.. automodule:: timeutils
    :members:
    :undoc-members:
    :show-inheritance:
