timeutils\.stopwatch module
===========================

.. automodule:: timeutils.stopwatch
    :members:
    :undoc-members:
    :show-inheritance:
