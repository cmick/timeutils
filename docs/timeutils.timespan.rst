timeutils\.timespan module
==========================

.. automodule:: timeutils.timespan
    :members:
    :show-inheritance:
    :inherited-members:
    :special-members: __str__
